//Cчётчик
window.onload = () =>{
    const button1 = document.querySelector("button"), button2 = document.querySelector("div > button + button"),
    button3 = document.querySelector("div > button + button + button"), span1 = document.querySelector("span"), 
    span2 = document.querySelector("div > span + span"),  span3 = document.querySelector("div > span + span + span");
    button1.id = "button-start";
    button2.id = "button-stop";
    button3.id = "button-reseat";
    span1.id = "span-minut";
    span2.id = "span-sec";
    span3.id = "span-milisec";

    let counter1 = 1, counter2 = 1, counter3 = 1, intervalHandler, intervalHandler2, intervalHandler3;
    const time = id => document.getElementById(id);

    const count1 = () =>{
        if(counter1 < 60){
            time("span-minut").innerText = counter1;
            counter1++;
        }else{
            counter1 = 0;
        }
    }

    const count2 = () =>{
        if(counter2 < 60){
            time("span-sec").innerText = counter2;
            counter2++;
        }else{
            counter2 = 0;
        }
    }

    const count3 = () =>{
        if(counter3 < 60){
            time("span-milisec").innerText = counter3;
            counter3++;
        }else{
            counter3 = 0;
        }
    }

    time("button-start").onclick = () => {
        intervalHandler = setInterval(count1, 3600000);
        intervalHandler2 = setInterval(count2, 60000);
        intervalHandler3 = setInterval(count3, 1000);
        const div1 = document.querySelector("div");
        div1.classList.add("green");
        div1.classList.remove("black","red");
    }
   time("button-stop").onclick = () => {
        clearInterval(intervalHandler);
        clearInterval(intervalHandler2);
        clearInterval(intervalHandler3);
        const div2 = document.querySelector("div")
        div2.classList.add("red");
        div2.classList.remove("green","silver");
   }
   time("button-reseat").onclick = () => {
        clearInterval(intervalHandler);
        clearInterval(intervalHandler3);
        clearInterval(intervalHandler2);
        counter1 = "00"
        counter2 = "00";
        counter3 = "00";
        count1();
        count2();
        count3();
        const div3 = document.querySelector("div");
        div3.classList.add("silver");
        div3.classList.remove("red")
   }

   //Проверка номера телефона
    const div4 = document.createElement("div");
    div4.id = "block-style";
    document.body.append(div4);

    const p1 = document.createElement("p");
    p1.id = "text-style";
    p1.textContent = "Введите ваш номер телефона: "
    div4.append(p1);

    const input = document.createElement("input");
    input.type = "tel";
    input.id = "input-style";
    input.placeholder = "000-000-00-00";
    div4.append(input);

    const button4 = document.createElement("button")
    button4.id = "button-submit";
    button4.textContent = "Отправить на проверку";
    div4.append(button4);

    document.getElementById("button-submit").onclick = () =>{
        const div5 = document.createElement("div");
        div5.textContent = `${input.value}`;

        let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
        if(pattern.test(div5.textContent)){
            document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg"
        }else{
           const p2 = document.createElement("p");
           p2.textContent = "Ошибка";
           document.body.append(p2);
        }
    }
    
//Слайдер
    const block = document.createElement("div");
    block.style.borderRadius = "10px";
    block.style.width = "30%";
    block.id = "block-style2";
    document.body.append(block);

    const img1 = document.createElement("img")
    img1.id = "transition";
    img1.src = "https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg";
    img1.style.width = "100%";
    block.append(img1);

    const img2 = document.createElement("img")
    img2.id = "transition";
    img2.src = "https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg";
    img2.style.width = "100%";

    const img3 = document.createElement("img")
    img3.id = "transition";
    img3.src = "https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg";
    img3.style.width = "100%";
    
    const img4 = document.createElement("img")
    img4.id = "transition";
    img4.src = "https://naukatv.ru/upload/files/shutterstock_418733752.jpg";
    img4.style.width = "100%";

    const img5 = document.createElement("img")
    img5.id = "transition";
    img5.src = "https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg";
    img5.style.width = "100%";

    const images2 = [img1.src, img2.src, img3.src, img4.src, img5.src];
    let nextImg = 0;

   setInterval(() =>{
        img1.src = images2[nextImg++ % images2.length]
    }, 1000)
}
    